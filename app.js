var express = require('express'),
	app = express(),
	server  = require('http').createServer(app),
	formatter = require('./formatterExp');

var history = []

app.use(express.json());       // to support JSON-encoded bodies
app.use(express.urlencoded({ extended: true })); // to support URL-encoded bodies
app.set('view engine', 'ejs');

app.get('/', function (request, response){
 	response.render("index",{texts: history});
});

app.post('/search', function (request, response) {
	if (request.body.name != ""){
		history.push(formatter(request.body.name))
		response.status(200).render("index",{texts: history});
	} else {
		response.render("index",{texts: history});
	}
});

server.listen(8001, function(){
  console.log('listening in http://localhost:8001');
});






