
var brands = require("./Models/Brands");
var type = require("./Models/TypeCloth");

var formated = function(text){
	var formatedText = text;
	for (brand of brands.sort(orderingByLegth).reverse()){
		if(text.toLowerCase().indexOf(brand.toLowerCase()) >= 0){
			formatedText = formatedText.replace(new RegExp(brand,"i") ,"<b>"+brand+"</b>");
			break
		}
	}

	for (type of type.sort(orderingByLegth).reverse()){
		if( text.toLowerCase().indexOf(type.toLowerCase()) >= 0){
			formatedText = formatedText.replace(new RegExp(type,"i") ,"<i>"+type+"</i>");
			break
		}
	}
	return formatedText;
}

var orderingByLegth = function (a, b) {
  if (a.length < b.length) {
    return -1;
  } else if (a.length > b.length) {
    return 1;
  }
  return 0;
}

module.exports = formated;